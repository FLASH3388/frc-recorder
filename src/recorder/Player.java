package recorder;

public interface Player {
    void play(Frame frame);
    String getName();
}
