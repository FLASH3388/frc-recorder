package recorder;

public interface Recorder {
    Frame capture();
    String getName();
    }
